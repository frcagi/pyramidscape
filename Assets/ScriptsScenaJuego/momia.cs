﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class momia : MonoBehaviour
{
    private float speed = 2f;
    private float verticalSpeed = 15f;
    private bool onGround;
    private Rigidbody rb;
    Animator anim;
    private RaycastHit hit;




    // Start is called before the first frame update
    void Start()
    {
        onGround = true;
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();

    }

    // Update is called once per frame
    void FixedUpdate()
    {
        rb.velocity = Vector3.right * speed;
        

    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.tag.ToLower().Equals("floortrap"))
        {
            if (onGround)
            {
                rb.AddForce((Vector3.up * verticalSpeed), ForceMode.Impulse);
                onGround = false;
                anim.SetBool("verticalSpeed", true);

            }
            if (collision.gameObject.tag.ToLower().Equals("ground"))
            {
                onGround = true;
                anim.SetBool("verticalSpeed", false);
            }
        }
    }
    private void onTriggerEnter(Collider collision)
    {
        if (collision.gameObject.tag.ToLower().Equals("trap") || collision.gameObject.tag.ToLower().Equals("airtrap") )
        {

            if (onGround)
            {
                jump();

            }
            if (collision.gameObject.tag.ToLower().Equals("ground"))
            {
                onGround = true;
                anim.SetBool("verticalSpeed", false);
            }
        }
    }
    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag.ToLower().Equals("ground")) {
            jump();
        }
    }
    private void Update()
    {
        /*  int layerMask = 1 << 8;

          // This would cast rays only against colliders in layer 8.
          // But instead we want to collide against everything except layer 8. The ~ operator does this, it inverts a bitmask.
          layerMask = ~layerMask;
          if (Physics.Raycast(transform.position, transform.TransformDirection(Vector3.right), out hit, Mathf.Infinity, layerMask))
          {
              if (hit.distance<5f)
              {
                  Debug.Log(hit.collider.gameObject.tag);
              }
              Debug.DrawRay(transform.position, transform.TransformDirection(Vector3.forward) * hit.distance, Color.yellow); 
              Debug.Log("I hit something ");   
          }*/
        Debug.DrawRay(transform.position, Vector3.right* 5f, Color.red);
        if(Physics.Raycast(transform.position, Vector3.right,out hit, 5f))
        {
            if (hit.transform.gameObject.tag.ToLower().Equals("airtramp") || hit.transform.gameObject.tag.ToLower().Equals("floortramp")) {
                jump();
                Debug.Log("Me via saltar");
            }
            
        }
    }
    void jump() {
        if (onGround)
        {
            rb.AddForce((Vector3.up * verticalSpeed), ForceMode.Impulse);
            onGround = false;
            anim.SetBool("verticalSpeed", true);

        }
    }

}
