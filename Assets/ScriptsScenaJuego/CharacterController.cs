﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class CharacterController : MonoBehaviour
{
    private Rigidbody rb;
    public float speedHorizontal = 5;
    public float jumpForce = 5;
    private bool isGrounded = true;
    private float timeJumpCount;
    private bool isJumping;
    private int nLifes = 3;
    public float jumpTime = 0.3f;
    public GameObject momiaPrefab;
    bool faceRight = true;
    private GameManager gm;
    private Animator anim;
    Transform transf;
    private GameObject momia;
    public Text textoMomia;
    private void Awake()
    {
        rb = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
        transf = GetComponent<Transform>();
       

    }
    // Start is called before the first frame update
    void Start()
    {
        gm = GameObject.FindGameObjectWithTag("gameManager").GetComponent<GameManager>();



    }

    // Update is called once per frame
    void Update()
    {
      

        if (isGrounded && Input.GetButtonDown("Jump"))
        {
            isJumping = true;
            anim.SetBool("isGrounded", false);
            //anim.setF
            isGrounded = false;
            timeJumpCount = jumpTime;
            rb.velocity = Vector3.up * jumpForce;
            anim.SetBool("vertical", true);
        }
        if ((Input.GetKey(KeyCode.Space) || Input.GetButton("Jump")) && isJumping)
        {
            if (timeJumpCount > 0)
            {
                anim.SetBool("vertical", true);
                rb.velocity = Vector3.up * jumpForce;
                timeJumpCount -= Time.deltaTime;
               
                anim.SetFloat("Speed",0);
            }
            else
            {
                isJumping = false;
             
            }
        }
        if (Input.GetKeyUp(KeyCode.Space))
        {
            isJumping = false;
        
        }
        
    }
  
    private void FixedUpdate()
    {
        float inputValue = Input.GetAxis("Horizontal");

        if (inputValue != 0)
        {
            if (inputValue < 0 && faceRight)
            {
                girar();
            }
            else
            {
                if (inputValue > 0 && !faceRight)
                {
                    girar();
                }
            }
            rb.velocity = new Vector3(speedHorizontal * inputValue, rb.velocity.y, 0f);
            anim.SetFloat("Speed", Mathf.Abs(inputValue));

        }
    }
    private void OnCollisionEnter(Collision collision)
    {
       //TESTEO CON RAYCAST
        if (collision.gameObject.tag.ToLower().Equals("ground") || collision.gameObject.tag.ToLower().Equals("airtrap")
            || collision.gameObject.tag.ToLower().Equals("airtrapmid"))
        {
            isGrounded = true;
            anim.SetBool("isGrounded", true);
            isJumping = false;
            anim.SetBool("vertical", false);
            
        }

        switch (collision.gameObject.tag.ToLower())
        {

            case "trap":
            case "momia":
            case "floortrap":
            case "walltraps":
                Invoke("killMainCharacter", 0f);

                break;
            case "end":
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
                break;

        }


    }

    private void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag.ToLower().Equals("airtrap"))
        {
            CancelInvoke("killMainCharacter");
        }
    }
    void killMainCharacter()
    {
        transform.position = new Vector3(-0f, -0f, 0f);
        gm.NLifes -= 1;
        if (momia!=null) {           
            Destroy(momiaPrefab);
            momiaPrefab = momia;
        }


    }
    IEnumerator invocarMomia()
    {
        textoMomia.text = "¡Has despertado a la momia, corre por tu vida!"; yield return new WaitForSeconds(2f);
        Instantiate(momiaPrefab, new Vector3(24.39f, 0f, 0f), Quaternion.Euler(0f, 90f, 0f));        
        momia = GameObject.FindGameObjectWithTag("momia");
        textoMomia.text = "";

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag.ToLower().Equals("sarcofago"))
        {

            StartCoroutine("invocarMomia");
        }
        if (other.gameObject.tag.ToLower().Equals("floortrap")||other.gameObject.tag.ToLower().Equals("rooftrap")|| other.gameObject.tag.ToLower().Equals("walltraps")) {
            Invoke("killMainCharacter", 0f);
        }
        if (other.gameObject.tag.ToLower().Equals("end"))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex+1);
        }

    }
    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.tag.ToLower().Equals("airtrap") || collision.gameObject.tag.ToLower().Equals("airtrapmid"))
        {
            //collision.gameObject.GetComponent<Rigidbody>().velocity = Vector3.down * 5f * Time.deltaTime;
            //Destroy(collision.gameObject, 5f);
            Invoke("killMainCharacter", 5f);




        }
    }
    private void girar()
    {
        transform.localScale = new Vector3((transform.localScale.x * -1), transform.localScale.y, transform.localScale.z * -1);
        faceRight = !faceRight;

    }
}
