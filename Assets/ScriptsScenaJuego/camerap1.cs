﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class camerap1 : MonoBehaviour
{
    public Transform target;
    public float smoothSpeed= 0.125f;
    public Vector3 offset;
    // Start is called before the first frame update
    void Start()
    {
        offset = new Vector3(1f, 3f, -8f);
    }

    // Update is called once per frame
    void LateUpdate()
    {
        if (target!=null) {
            transform.position = target.position + offset;
        }
    }
}
