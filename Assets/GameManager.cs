﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    GameObject character;
    GameObject[] floortraps;
    public List<GameObject> heart;
    GameObject[] rooftrap;
    GameObject[] walltraps;
    bool areoutside = false;
    bool areup = true;
    private int nLifes = 5;

    public int NLifes { get => nLifes; set => nLifes = value; }

    private void Awake()
    {
        // heartObjects = GameObject.FindGameObjectsWithTag("life");
        floortraps = GameObject.FindGameObjectsWithTag("floortrap");
        walltraps = GameObject.FindGameObjectsWithTag("walltraps");
        rooftrap = GameObject.FindGameObjectsWithTag("rooftrap");
        InvokeRepeating("movetraps", 0f, 5f);
        character = GameObject.FindGameObjectWithTag("exploradora");

    }
    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        switch (nLifes)
        {
            case 0:
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1); break;
            case 1:
                heart[1].SetActive(false);
                break;
            case 2:
                heart[2].SetActive(false);
                break;
            case 3:
                heart[3].SetActive(false); break;
            case 4:
                heart[4].SetActive(false); break;
        }
    }
    void movetraps()
    {
        foreach (GameObject go in floortraps)
        {
            Debug.Log(" HE detectado trampas del suelo:  "+floortraps.Length);
            if (areup)
            {
                go.GetComponent<Rigidbody>().velocity = Vector3.down*0.8f;

            }
            else if (!areup)
            {
                go.GetComponent<Rigidbody>().velocity = Vector3.up * 0.8f;
            }
            Mathf.Clamp(go.GetComponent<Transform>().position.y, go.GetComponent<Rigidbody>().transform.position.y - 10f, go.GetComponent<Rigidbody>().transform.position.y + 10f);

        }
      
        Debug.Log("Areup ha cambiado a " + areup);

        Debug.Log("Are outside? " + areoutside);
        foreach (GameObject go in walltraps)
        {
            if (!areoutside)
            {

                go.GetComponent<Rigidbody>().velocity = Vector3.back*0.5f;

            }
            else
            if (areoutside)
            {
                go.GetComponent<Rigidbody>().velocity = Vector3.forward*0.5f;

            }
        }
       
        Debug.Log("areoutside cambiado a " + areup);
        foreach (GameObject go in rooftrap)
        {
            if (!areoutside)
            {
                Debug.Log("Voy a rotar la trampa");
                go.GetComponent<Rigidbody>().velocity = Vector3.down * 0.5f;
                     
                Quaternion deltaRotation = Quaternion.Euler(rotateTramps() * Time.deltaTime);
                go.GetComponent<Rigidbody>().MoveRotation(go.GetComponent<Rigidbody>().rotation * deltaRotation);



            }
            else
            if (areoutside)
            {
                Debug.Log("Voy a volver a rotar la trampa");
                go.GetComponent<Rigidbody>().velocity = Vector3.up * 0.5f;
                Quaternion deltaRotation = Quaternion.Euler((rotateTramps()*-1) * Time.deltaTime);
                go.GetComponent<Rigidbody>().MoveRotation(go.GetComponent<Rigidbody>().rotation * deltaRotation);

            }
        }
        areup = !areup;
        areoutside = !areoutside;
    }
   Vector3 rotateTramps()
    {
       return  new Vector3(180, 0,270);
    }


}
